package gogcs

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"cloud.google.com/go/storage"
)

func (s GoGSCClient) UploadFiles(files []File) ([]UploadedFile, error) {
	bh := s.StorageClient.Bucket(s.Bucket).UserProject(s.ProjectID)
	var results []UploadedFile
	for _, file := range files {
		obj := bh.Object(GetFullPath(file.Path, file.Name))
		w := obj.NewWriter(s.Context)

		if _, err := io.Copy(w, file.Body); err != nil {
			return results, err
		}
		if err := w.Close(); err != nil {
			return results, err
		}
		if file.IsPublic {
			if err := obj.ACL().Set(s.Context, storage.AllUsers, storage.RoleReader); err != nil {
				return results, err
			}
		}
		objAttrs, err := obj.Attrs(s.Context)
		if objAttrs == nil {
			return results, err
		}
		results = append(results, UploadedFile{
			Name:        file.Name,
			Size:        objAttrs.Size,
			IsPublic:    file.IsPublic,
			MD5:         MD5BytesToString(objAttrs.MD5),
			Url:         ObjectToUrl(s.BaseUrl, objAttrs),
			ObjectAttrs: objAttrs,
		})
	}
	return results, nil
}

func (s GoGSCClient) downloadFile(download DownloadedFile) (*DownloadedFile, error) {
	rc, err := s.StorageClient.Bucket(s.Bucket).Object(download.Object).NewReader(s.Context)
	if err != nil {
		return nil, err
	}
	defer func() {
		err := rc.Close()
		if err != nil {
			panic(fmt.Errorf("error 2 %v", err))
		}
	}()
	data, err := ioutil.ReadAll(rc)
	if err != nil {
		return nil, err
	}
	err = ioutil.WriteFile(GetFullPath(download.Path, download.Name), data, 0644)
	if err != nil {
		return nil, err
	}
	download.Data = &data
	return &download, nil
}

func (s GoGSCClient) removeFile(download DownloadedFile) error {
	object := s.StorageClient.Bucket(s.Bucket).Object(download.Object)
	if err := object.Delete(s.Context); err != nil {
		return err
	}
	return nil
}

func (s GoGSCClient) DownloadFiles(downloads []DownloadedFile) error {
	defer func() {
		err := s.StorageClient.Close()
		if err != nil {
			panic(fmt.Errorf("error during closing connection: %v", err))
		}
	}()
	for k, download := range downloads {
		result, err := s.downloadFile(download)
		if err != nil {
			return err
		}
		downloads[k].Data = result.Data
	}
	return nil
}

func (s GoGSCClient) RemoveFiles(downloads []DownloadedFile) error {
	defer func() {
		err := s.StorageClient.Close()
		if err != nil {
			panic(fmt.Errorf("error during closing connection: %v", err))
		}
	}()
	for _, download := range downloads {
		err := s.removeFile(download)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s GoGSCClient) GenerateImageResizable(ctx context.Context, image string) (*string, error) {
	var imageResizableDTO struct {
		ImageUrl string `json:"image_url"`
	}
	resizeUrl := fmt.Sprintf("https://%s.appspot.com/image-url", s.ProjectID)
	req, err := http.NewRequest("GET", resizeUrl, nil)
	if err != nil {
		return nil, err
	}
	q := req.URL.Query()
	q.Add("bucket", s.Bucket)
	q.Add("image", image)
	req.URL.RawQuery = q.Encode()
	req = req.WithContext(ctx)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if err = json.Unmarshal(responseBody, &imageResizableDTO); err != nil {
		return nil, err
	}
	return &imageResizableDTO.ImageUrl, nil
}

func (s GoGSCClient) FileAttrs(files []File) ([]FileAttr, error) {
	bh := s.StorageClient.Bucket(s.Bucket).UserProject(s.ProjectID)
	var results []FileAttr
	for _, file := range files {
		obj := bh.Object(GetFullPath(file.Path, file.Name))
		attr, err := obj.Attrs(s.Context)
		if err != nil {
			return results, err
		}
		results = append(results, FileAttr{
			MD5:  attr.MD5,
			Size: attr.Size,
		})
	}
	return results, nil
}
