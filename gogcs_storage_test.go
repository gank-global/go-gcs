package gogcs

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"testing"
	"time"
)

func TestGetFullPath(t *testing.T) {
	t.Run("It should get full path", func(t *testing.T) {
		actualFileName := GetFullPath("/file/path", "file.txt")
		expectedFileName := "/file/path/file.txt"
		if actualFileName != expectedFileName {
			t.Errorf("Expecting result to be %v got %v", expectedFileName, actualFileName)
		}
	})
}

func TestNewGCSClient(t *testing.T) {
	t.Run("It should instantiate new GCS Client", func(t *testing.T) {
		client, _ := NewGCSClient(context.Background())
		if client == nil {
			t.Error("Client is nil")
		}
	})
}

func TestGoGSCClient_UploadFiles(t *testing.T) {
	t.Run("It should upload one file", func(t *testing.T) {
		ctx := context.Background()
		client, _ := NewGCSClient(ctx)
		if client == nil {
			t.Error("Client is nil")
		}
		f, err := os.Open("sample.txt")
		if err != nil {
			log.Fatal(err)
			return
		}
		file := File{
			Name:     "test.txt",
			Path:     "new/test/file",
			Body:     f,
			IsPublic: true,
		}
		var files []File
		files = append(files, file)
		result, err := client.UploadFiles(files)

		if err != nil {
			t.Errorf("error not nil: %v", err)
		}

		if result == nil {
			t.Error("Result should not be nil!")
		}
	})
}

func TestGoGSCClient_DownloadFile(t *testing.T) {
	t.Run("It should download single/Multiple file", func(t *testing.T) {

		ctx := context.Background()
		ctx, cancel := context.WithTimeout(ctx, time.Second*50)
		defer cancel()
		client, _ := NewGCSClient(ctx)
		if client == nil {
			t.Error("Client is nil")
			return
		}
		downloadFiles := []DownloadedFile{
			{
				Object: "new/test/file/test.txt",
				Name:   "test.txt",
				Path:   "",
			},
		}
		err := client.DownloadFiles(downloadFiles)

		if err != nil {
			t.Error("error not nil")
		}
	})
}

func TestGoGSCClient_RemoveFile(t *testing.T) {
	t.Run("It should download single/multiple file", func(t *testing.T) {
		ctx := context.Background()
		ctx, cancel := context.WithTimeout(ctx, time.Second*50)
		defer cancel()
		client, _ := NewGCSClient(ctx)
		if client == nil {
			t.Error("Client is nil")
			return
		}
		downloadFiles := []DownloadedFile{
			{
				Object: "new/test/file/test.txt",
				Name:   "test.txt",
				Path:   "",
			},
		}
		err := client.RemoveFiles(downloadFiles)

		if err != nil {
			t.Error("error not nil")
		}
	})
}

func TestGoGSCClient_DownloadFiles(t *testing.T) {
	req, err := http.NewRequest("GET", "https://gank-staging-276406.appspot.com/image-url", nil)
	if err != nil {
		fmt.Printf("[Error] %v \n", err)
		return
	}
	q := req.URL.Query()
	q.Add("bucket", "gank-staging")
	q.Add("image", "media/b6cdf9c4-939d-48ca-9574-c38679cbeae6/Referral-1.jpg")
	req.URL.RawQuery = q.Encode()

	// TODO: check err
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("[Error] %v \n", err)
		return
	}

	var imageResizableDTO struct {
		ImageUrl string `json:"image_url"`
	}
	defer resp.Body.Close()
	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("[Error] %v \n", err)
		return
	}
	if err = json.Unmarshal(responseBody, &imageResizableDTO); err != nil {
		fmt.Printf("[Error] %v \n", err)
		return
	}
	fmt.Printf("[Result] %v", imageResizableDTO)
}

func TestGoGSCClient_GenerateImageResizable(t *testing.T) {
	_ = os.Setenv("GCS_BUCKET", "gank-staging")
	_ = os.Setenv("GCS_PROJECT_ID", "gank-staging-276406")
	_ = os.Setenv("GCS_BASE_URL", "https://cdn-staging.ganknow.com")
	_ = os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", "./gank-staging.json")
	ctx := context.Background()
	gscClient, err := NewGCSClient(ctx)
	if err != nil {
		fmt.Printf("[Error] Init gcs client %v \n", err)
		return
	}

	imageUrl := "media/83ac45f6-e87b-4363-9b41-a3a8a4bc1007/130038966_3458103027637470_4273497989322791239_n.jpg"
	imageResizable, err := gscClient.GenerateImageResizable(ctx, imageUrl)
	if err != nil {
		fmt.Printf("[Error] generate image resizable %v \n", err)
		return
	}
	fmt.Println(*imageResizable)

}
