package gogcs

import (
	"context"
	"fmt"
	"os"
	"testing"
)


func TestGoGSCClient_CheckAdult(t *testing.T) {
	_ = os.Setenv("GCS_BUCKET", "gank-staging")
	_ = os.Setenv("GCS_PROJECT_ID", "gank-staging-276406")
	_ = os.Setenv("GCS_BASE_URL", "https://cdn-staging.ganknow.com")
	_ = os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", "./gank-staging.json")
	ctx := context.Background()
	gscClient, err := NewGCSClient(ctx)
	if err != nil {
		fmt.Printf("[Error] Init gcs client %v \n", err)
		return
	}

	file := "image.jpg"
	file = "images.jpeg"
	f, err := os.Open(file)
	if err != nil {
		fmt.Printf("[Error] can not open image %v \n", err)
		return
	}
	defer f.Close()
	isAdultContent, err := gscClient.ValidAdultContent(f, []string{LikelihoodsLikely, LikelihoodsVeryLikely})
	if err != nil {
		fmt.Printf("[Error] check adult content %v \n", err)
		return
	}
	fmt.Println(isAdultContent)
}
