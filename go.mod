module bitbucket.org/gank-global/go-gcs

go 1.12

require (
	cloud.google.com/go v0.57.0
	cloud.google.com/go/storage v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
)
