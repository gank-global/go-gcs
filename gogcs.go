package gogcs

import (
	"context"
	"io"

	"cloud.google.com/go/storage"
	vision "cloud.google.com/go/vision/apiv1"
)

type GoGCSClient interface {
	UploadFiles(files []File) ([]UploadedFile, error)
	DownloadFiles(downloads []DownloadedFile) error
	RemoveFiles(downloads []DownloadedFile) error
	GenerateImageResizable(ctx context.Context, image string) (*string, error)
	ValidAdultContent(body io.Reader, unAcceptLikelihoods []string) (bool, error)
	FileAttrs(files []File) ([]FileAttr, error)
}

type GoGSCClient struct {
	StorageClient        *storage.Client
	ImageAnnotatorClient *vision.ImageAnnotatorClient
	ProjectID            string
	Bucket               string
	BaseUrl              string
	Context              context.Context
}

func NewGCSClient(ctx context.Context) (*GoGSCClient, error) {
	config := LoadGSCConfig()
	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		return nil, err
	}
	imageAnnotatorClient, err := vision.NewImageAnnotatorClient(ctx)
	if err != nil {
		return nil, err
	}
	return &GoGSCClient{
		StorageClient:        storageClient,
		ImageAnnotatorClient: imageAnnotatorClient,
		ProjectID:            config.ProjectID,
		Bucket:               config.Bucket,
		BaseUrl:              config.BaseUrl,
		Context:              ctx,
	}, nil
}
