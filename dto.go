package gogcs

import (
	"io"

	"cloud.google.com/go/storage"
)

const (
	LikelihoodsUnKnown      = "UNKNOWN"
	LikelihoodsVeryUnLikely = "VERY_UNLIKELY"
	LikelihoodsUnLikely     = "UNLIKELY"
	LikelihoodsPossible     = "POSSIBLE"
	LikelihoodsLikely       = "LIKELY"
	LikelihoodsVeryLikely   = "VERY_LIKELY"
)

type File struct {
	Path     string
	Name     string
	Body     io.Reader
	IsPublic bool
}

type UploadedFile struct {
	Name        string
	MD5         string
	IsPublic    bool
	Url         string
	Size        int64
	ObjectAttrs *storage.ObjectAttrs
}

type DownloadedFile struct {
	Object string
	Name   string
	Path   string
	Data   *[]byte
}

type FileAttr struct {
	MD5  []byte
	Size int64
}
