package gogcs

import (
	vision "cloud.google.com/go/vision/apiv1"
	"io"
)

func (s GoGSCClient) ValidAdultContent(body io.Reader, unAcceptLikelihoods []string) (bool, error) {
	image, err := vision.NewImageFromReader(body)
	if err != nil {
		return false, err
	}
	props, err := s.ImageAnnotatorClient.DetectSafeSearch(s.Context, image, nil)
	if err != nil {
		return false, err
	}
	for _, unAcceptLikelihood := range unAcceptLikelihoods {
		if unAcceptLikelihood == props.Adult.String() {
			return true, nil
		}
	}
	return false, nil
}
